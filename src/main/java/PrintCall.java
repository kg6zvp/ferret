import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.List;

public class PrintCall {
    public static void print(Object o, Object...args) {
        System.out.printf("%s %s#%s\n",
                o.getClass().getName(),
                getStateAsString(o),
                getCallingMethod(o, args));
    }

    public static String getStateAsString(Object o) {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(o);
        } catch (JsonProcessingException e) {
            return "{ }";
        }
    }

    public static String getCallingMethod(Object o, Object...args) {
        String methodName = "";
        try {
            throw new RuntimeException();
        } catch(RuntimeException e) {
            StackTraceElement me = e.getStackTrace()[2];
            methodName = me.getMethodName();
        }
        StringBuilder argStringBuilder = new StringBuilder(methodName);
        argStringBuilder.append("(");
        List<String> argStrings = new LinkedList<>();
        for(Object k : args) {
            if (k instanceof String)
                argStrings.add(String.format("\"%s\"", k));
            else if(k instanceof Number)
                argStrings.add(String.valueOf(k));
            else if(k instanceof Enum)
                argStrings.add(k.getClass().getSimpleName() + "." + ((Enum)k).toString());
        }
        argStringBuilder.append(String.join(", ", argStrings));
        argStringBuilder.append(")");
        return argStringBuilder.toString();
    }
}
