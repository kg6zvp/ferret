import org.junit.jupiter.api.Test;

public class PrintCallTest {
    @Test
    public void testPrint() {
        PrintCall.print(this);
    }

    @Test
    public void testOtherMethod() {
        PrintCall.print(this, "hello", 5, 3.14, TEST_NUM.THREEVE);
    }

    public enum TEST_NUM {
        THREEVE
    }
}
